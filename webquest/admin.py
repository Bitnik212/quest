from django.contrib import admin

# Register your models here.
from webquest.models import Models

for model in Models:
    admin.site.register(model.value, model.value.AdminView)
