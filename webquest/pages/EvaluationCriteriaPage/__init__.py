from django.shortcuts import render
from django.views import View

from webquest.models import QuestResult, QuestUser


class EvaluationCriteriaView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.page_name = "evaluation_criteria.html"

    def get(self, request):
        fio_table_column = []
        data = {}
        columns_name = {"ФИО участника группы": 1}
        sub_columns_name = [""]
        users = QuestUser.objects.all()
        user_stats_list = []
        counter = 0
        for user in users:
            fio_table_column.append(user.fio)
            results = QuestResult.objects.filter(user=user)
            user_stats = {}
            stats_list = []
            stats_list.append(user.fio)
            for result in results:
                stats_list.append(result.result)
                criteria_group = user_stats.get(result.sub_criterion.group.name)
                if criteria_group is not None:
                    criteria_group.append({
                            result.sub_criterion.name: result.result
                        })
                    pass
                else:
                    user_stats.update({
                        result.sub_criterion.group.name: [
                            {result.sub_criterion.name: result.result}
                        ]
                    })
                    columns_name.update({result.sub_criterion.group.name: 1})
                if result.sub_criterion.name not in sub_columns_name:
                    sub_columns_name.append(result.sub_criterion.name)
            data.update({
                user.fio: user_stats
            })
            user_stats_list.append(stats_list)
            for column_name in columns_name:
                g = user_stats.get(column_name)
                if g is not None and isinstance(g[0], dict):
                    columns_name.update({
                        column_name: len(g)
                    })
            counter += 1
                # columns_name.get(column_name)
        print(data)
        print(fio_table_column)
        print(columns_name)
        print(user_stats_list)
        print(sub_columns_name)
        return render(request, self.page_name, context={
            "fio_table_column": fio_table_column,
            "data": data,
            "columns_name": columns_name,
            "sub_columns_name": sub_columns_name,
            "user_stats_list": user_stats_list
        })
