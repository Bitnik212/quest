from django.shortcuts import render
from django.views import View


class OperationProcedureView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.page_name = "operation_procedure.html"

    def get(self, request):
        # <view logic>
        return render(request, self.page_name)
