from django.shortcuts import render
from django.views import View


class HeraldistRoleView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.page_name = "role/heraldist.html"

    def get(self, request):
        # <view logic>
        return render(request, self.page_name)
