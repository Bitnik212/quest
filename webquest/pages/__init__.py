from quest.ViewEnum import ViewEnum
from webquest.pages.EcologistRolePage import EcologistRoleView
from webquest.pages.EvaluationCriteriaPage import EvaluationCriteriaView
from webquest.pages.GuideRolePage import GuideRoleView
from webquest.pages.HeraldistRolePage import HeraldistRoleView
from webquest.pages.HistorianRolePage import HistorianRoleView
from webquest.pages.IndexPage import IndexView
from webquest.pages.LocalHistorianRolePage import LocalHistorianRoleView
from webquest.pages.OperationProcedurePage import OperationProcedureView
from webquest.pages.ResultsPage import ResultsView
from webquest.pages.RolesPage import RolesView


class Views(ViewEnum):
    index = IndexView
    roles = RolesView
    operating_procedure = OperationProcedureView
    results = ResultsView
    heraldist = HeraldistRoleView
    local_historian = LocalHistorianRoleView
    historian = HistorianRoleView
    ecologist = EcologistRoleView
    guide = GuideRoleView
    evaluation_criteria = EvaluationCriteriaView
