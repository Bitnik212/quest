from django.apps import AppConfig


class WebquestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'webquest'
    icon_name = 'message'
