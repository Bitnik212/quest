from django import template

from webquest.models import QuestRole

register = template.Library()


@register.inclusion_tag("list_view/roles_description_list.html")
def show_roles_description():
    roles = QuestRole.objects.all()
    return {"roles": roles}


@register.inclusion_tag("list_view/roles_name_list.html")
def show_roles_name():
    roles = QuestRole.objects.all()
    return {"roles": roles}

