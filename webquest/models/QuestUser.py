from django.db import models
from django.contrib import admin

from webquest.models.QuestRole import QuestRole


class QuestUser(models.Model):

    class AdminView(admin.ModelAdmin):
        list_display = ['fio', "role"]
        ordering = ['fio']
        icon_name = "person"

    class Meta:
        verbose_name_plural = "Участники квеста"

    def __str__(self):
        return self.fio

    fio = models.TextField(verbose_name="ФИО участника")
    role = models.ForeignKey(
        QuestRole,
        on_delete=models.CASCADE,
        verbose_name="Роль",
        related_name="quest_user_role"
    )
