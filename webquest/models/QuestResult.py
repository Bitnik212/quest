from django.db import models
from django.contrib import admin

from webquest.models.QuestUser import QuestUser
from webquest.models.SubCriterion import SubCriterion


class QuestResult(models.Model):

    class AdminView(admin.ModelAdmin):
        list_display = ['user', "sub_criterion", "result"]
        ordering = ['user']
        icon_name = "view_list"

    class Meta:
        verbose_name_plural = "Результаты"

    def __str__(self):
        return str(self.user)

    user = models.ForeignKey(
        QuestUser,
        on_delete=models.CASCADE,
        verbose_name="Участник",
        related_name="quest_result_user"
    )
    sub_criterion = models.ForeignKey(
        SubCriterion,
        on_delete=models.CASCADE,
        verbose_name="Критерий",
        related_name="quest_result_criterion"
    )
    result = models.IntegerField(verbose_name="Оценка")
