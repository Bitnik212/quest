from django.db import models
from django.contrib import admin

from webquest.models.CriterionGroup import CriterionGroup


class SubCriterion(models.Model):

    class AdminView(admin.ModelAdmin):
        list_display = ['name', 'group']
        ordering = ['name']
        icon_name = "view_list"

    class Meta:
        verbose_name_plural = "Критерии"

    def __str__(self):
        return self.name

    name = models.TextField(verbose_name="Название критерия")
    group = models.ForeignKey(
        CriterionGroup,
        on_delete=models.CASCADE,
        verbose_name="Группа",
        related_name="sub_criterion_group"
    )
