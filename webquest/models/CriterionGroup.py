from django.db import models
from django.contrib import admin


class CriterionGroup(models.Model):

    class AdminView(admin.ModelAdmin):
        list_display = ['name']
        ordering = ['name']
        icon_name = "view_list"

    class Meta:
        verbose_name_plural = "Группы критериев"

    def __str__(self):
        return self.name

    name = models.TextField(verbose_name="Название группы")
