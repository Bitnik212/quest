from enum import Enum

from webquest.models.CriterionGroup import CriterionGroup
from webquest.models.QuestResult import QuestResult
from webquest.models.QuestRole import QuestRole
from webquest.models.QuestRoleTask import QuestRoleTasks
from webquest.models.QuestRoleTaskInfoSource import QuestRoleTaskInfoSource
from webquest.models.QuestUser import QuestUser
from webquest.models.SubCriterion import SubCriterion


class Models(Enum):
    group = CriterionGroup
    sub = SubCriterion
    role = QuestRole
    user = QuestUser
    # role_task = QuestRoleTasks
    result = QuestResult
    # role_task_info_source = QuestRoleTaskInfoSource
