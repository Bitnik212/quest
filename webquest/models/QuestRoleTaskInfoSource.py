from django.db import models
from django.contrib import admin

from webquest.models import QuestRole


class QuestRoleTaskInfoSource(models.Model):

    class AdminView(admin.ModelAdmin):
        list_display = ['url', 'role']
        ordering = ['role']
        icon_name = "view_list"

    class Meta:
        verbose_name_plural = "Ссылки для задач роли"

    def __str__(self):
        return self.url

    url = models.TextField(verbose_name="Ссылка")
    role = models.ForeignKey(
        QuestRole,
        on_delete=models.CASCADE,
        verbose_name="Роль",
        related_name="quest_role_task_info_source_role"
    )
