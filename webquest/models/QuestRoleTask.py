from django.db import models
from django.contrib import admin

from webquest.models.QuestRole import QuestRole


class QuestRoleTasks(models.Model):

    class AdminView(admin.ModelAdmin):
        list_display = ['task', "role"]
        ordering = ['task']
        icon_name = "view_list"

    class Meta:
        verbose_name_plural = "Задачи квеста"

    def __str__(self):
        return self.task

    task = models.TextField(verbose_name="Задача")
    role = models.ForeignKey(
        QuestRole,
        on_delete=models.CASCADE,
        verbose_name="Роль",
        related_name="quest_role_task"
    )
