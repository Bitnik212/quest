from django.db import models
from django.contrib import admin


class QuestRole(models.Model):

    class AdminView(admin.ModelAdmin):
        list_display = ['name']
        ordering = ['name']
        icon_name = "group"

    class Meta:
        verbose_name_plural = "Роли квеста"

    def __str__(self):
        return self.name

    name = models.TextField(verbose_name="Название")
    description = models.TextField(verbose_name="Описание")
    url = models.TextField(verbose_name="Ссылка на роль")
