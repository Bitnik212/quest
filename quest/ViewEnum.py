from enum import Enum, unique
from django.views import View
from django.urls import path
from django.contrib.auth import logout
from django.shortcuts import redirect


@unique
class ViewEnum(Enum):
    @staticmethod
    def logout_user(request):
        logout(request)
        return redirect('signin')

    @staticmethod
    def fix_names(name: str) -> str:
        match name:
            case "index":
                return ""
            case _:
                return name + "/"

    @classmethod
    def list(cls) -> [path]:
        return [
            path(
                cls.fix_names(item.name),
                item.value.as_view(),
                name=item.name
            ) for item in list(cls) if issubclass(item.value, View)
        ]
