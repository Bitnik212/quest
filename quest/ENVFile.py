from enum import Enum
from pathlib import Path

import environ


class ENVFile:
    class DBEngineType(Enum):
        SQLITE = 'django.db.backends.sqlite3'
        POSTGRES = 'django.db.backends.postgresql_psycopg2'

    def __init__(self, base_dir: Path or None = None):
        if base_dir is None:
            base_dir = Path(__file__).resolve().parent.parent
        self.base_dir = base_dir
        self.env_file_path = base_dir / ".env"
        environ.Env.read_env(self.env_file_path)
        self.env_file = environ.Env()

    @property
    def DEBUG(self) -> bool:
        return self.env_file("APP_DEBUG") == "true"

    @property
    def SECRET_KEY(self) -> str:
        return self.env_file("APP_SECRET_KEY")

    def DATABASE_ENGINE(self, for_test: bool = False) -> DBEngineType:
        if for_test:
            engine_string = self.env_file("TEST_DATABASE_ENGINE")
        else:
            engine_string = self.env_file("DATABASE_ENGINE")
        if engine_string == "sqlite":
            return ENVFile.DBEngineType.SQLITE
        elif engine_string == "postgres":
            return ENVFile.DBEngineType.POSTGRES

    def DATABASE_TABLE(self, for_test: bool = False) -> str:
        if for_test:
            return self.env_file("TEST_DATABASE_TABLE")
        else:
            return self.env_file("DATABASE_TABLE")

    def DATABASE_USER(self, for_test: bool = False) -> str:
        if for_test:
            return self.env_file("TEST_DATABASE_USER")
        else:
            return self.env_file("DATABASE_USER")

    def DATABASE_PASSWORD(self, for_test: bool = False) -> str:
        if for_test:
            return self.env_file("TEST_DATABASE_PASSWORD")
        else:
            return self.env_file("DATABASE_PASSWORD")

    def DATABASE_PORT(self, for_test: bool = False) -> int:
        if for_test:
            return int(self.env_file("TEST_DATABASE_PORT"))
        else:
            return int(self.env_file("DATABASE_PORT"))

    def DATABASE_HOST(self, for_test: bool = False) -> str:
        if for_test:
            return self.env_file("TEST_DATABASE_HOST")
        else:
            return self.env_file("DATABASE_HOST")

    @property
    def DATABASE(self) -> dict[str, str]:
        database_engine = self.DATABASE_ENGINE()
        database_engine_string: str = database_engine.value
        if database_engine == ENVFile.DBEngineType.POSTGRES:
            return {
                   'ENGINE': database_engine_string,
                   'NAME': self.DATABASE_TABLE(),
                   'USER': self.DATABASE_USER(),
                   'PASSWORD': self.DATABASE_PASSWORD(),
                   'HOST': self.DATABASE_HOST(),
                   'PORT': str(self.DATABASE_PORT()),
                   'TEST': self.__TEST_DATABASE
               }
        elif database_engine == ENVFile.DBEngineType.SQLITE:
            return {
                   'ENGINE': database_engine_string,
                   'NAME': self.base_dir / "db.sqlite3",
                   'TEST': self.__TEST_DATABASE
               }

    @property
    def __TEST_DATABASE(self) -> dict[str, str]:
        database_engine = self.DATABASE_ENGINE(for_test=True)
        database_engine_string: str = database_engine.value
        if database_engine == ENVFile.DBEngineType.POSTGRES:
            return {
                'ENGINE': database_engine_string,
                'NAME': self.DATABASE_TABLE(for_test=True),
                'USER': self.DATABASE_USER(for_test=True),
                'PASSWORD': self.DATABASE_PASSWORD(for_test=True),
                'HOST': self.DATABASE_HOST(for_test=True),
                'PORT': str(self.DATABASE_PORT(for_test=True)),
            }
        elif database_engine == ENVFile.DBEngineType.SQLITE:
            return {
                'ENGINE': database_engine_string,
                'NAME': str(self.base_dir / "test_db.sqlite3"),
            }
