from django.conf import settings
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static

from webquest.pages import Views as MainViews

urlpatterns = [
    path('admin/', admin.site.urls),
] + MainViews.list()
